//============================================================================
// Name        : simpletest0.cpp
// Author      : 
// Version     :
// Copyright   : CERN
// Description : reconnection tests for all vendors
//============================================================================

#include <iostream>

#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>
#include <LogIt.h>


using namespace std;

void usage( string ar ){
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << ar << " [systec || anagate || peak]" << endl;
	exit(0);
}

/**
 * testing CanModule for sw disconnect and reconnect, and for double connection/disconnection.
 * HW reconnect is NOT tested here.
 */
int main( int argc, char **argv ) {

	bool systec_flag = false;
	bool anagate_flag = false;
	bool peak_flag = false;

	if ( argc < 2 ) usage( argv[0] );
	for ( int i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "systec")) systec_flag = true;
		if ( 0 == strcmp( argv[i], "anagate")) anagate_flag = true;
		if ( 0 == strcmp( argv[i], "peak")) peak_flag = true;
	}
	CanModule::CCanAccess *_cca0 = NULL;
	CanModule::CCanAccess *_cca1 = NULL;
	CanModule::CCanAccess *_cca2 = NULL;
	CanModule::CanLibLoader *_libloader = NULL;

	Log::LOG_LEVEL loglevel = Log::TRC;
	Log::initializeLogging( loglevel );
	LogItInstance *logIt = LogItInstance::getInstance();

	cout << __FILE__ << " " << __LINE__ << " register log it" << endl;
	logIt->registerLoggingComponent("CanModule", Log::TRC );

	cout << __FILE__ << " " << __LINE__ << " load lib" << endl;

#ifdef _WIN32
	if ( systec_flag && !anagate_flag){
		// windows systec
		_libloader = CanModule::CanLibLoader::createInstance( "st" );
	} else if ( peak_flag && !anagate_flag){
		// windows peak
		_libloader = CanModule::CanLibLoader::createInstance( "pk" );

	}
#else
	if ( (systec_flag || peak_flag) && !anagate_flag ){
		// linux systec & peak
		_libloader = CanModule::CanLibLoader::createInstance( "sock" );
	}
#endif

	if ( anagate_flag && (!systec_flag && !peak_flag) ){
		// linux & windows anagate
		_libloader = CanModule::CanLibLoader::createInstance( "an" );
	}

	for ( int i = 100; i > 0; i--){
		cout << endl << "===" << __FILE__ << " " << __LINE__ << " " << i << " (re-)create bus" << endl;
#ifdef _WIN32
		if ( systec_flag && !anagate_flag ){
			// windows systec
			_cca0 = _libloader->openCanBus( "st:can0", "125000" ); //sender
			_cca1 = _libloader->openCanBus( "st:can1", "125000" ); // receiver
			_cca2 = _libloader->openCanBus( "st:can0", "125000" ); //sender same bus
		} else if ( peak_flag && !anagate_flag ){
			// windows peak
			_cca0 = _libloader->openCanBus( "pk:can0", "Unspecified" );
			_cca1 = _libloader->openCanBus( "pk:can1", "Unspecified" ); // sender
			_cca2 = _libloader->openCanBus( "pk:can0", "Unspecified" ); // same bus
		}
#else
		if ( systec_flag || peak_flag ){
			// linux systec & peak
			_cca0 = _libloader->openCanBus( "sock:can0", "125000" ); // sender
			_cca1 = _libloader->openCanBus( "sock:can1", "125000" ); // receiver
			_cca2 = _libloader->openCanBus( "sock:can1", "125000" ); // receiver open same bus again
		}
#endif
		if ( anagate_flag && (!systec_flag && !peak_flag) ){
			// linux & windows anagate
			_cca0 = _libloader->openCanBus( "an:can0:128.141.159.194", "125000 0 0 0 0" ); //sender
			_cca1 = _libloader->openCanBus( "an:can1:128.141.159.194", "125000 0 0 0 0" ); // receiver
			_cca2 = _libloader->openCanBus( "an:can0:128.141.159.194", "125000 0 0 0 0" ); //sender same bus
		}

		CanMessage cmsg;
		cmsg.c_id = 12;
		cout << __FILE__ << " " << __LINE__ << " send msg" << endl;
		for ( int k = 2; k > 0; k--){
			_cca0->sendMessage( &cmsg );
			_cca1->sendMessage( &cmsg );
		}

#ifdef _WIN32
		Sleep( 2000 );
#else
		sleep( 2 );
#endif

		cout << __FILE__ << " " << __LINE__ << " " << i << " explicitly close buses" << endl;
		_libloader->closeCanBus( _cca0);
		_libloader->closeCanBus( _cca1);
		_libloader->closeCanBus( _cca2); // delete same bus again

#ifdef _WIN32
		Sleep( 2000 );
#else
		sleep( 2 );
#endif
	}
	return 0;
}
