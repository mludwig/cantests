/*
 * SpecialCommands.h
 *
 *  Created on: Feb 21, 2019
 *      Author: mludwig
 */

#ifndef SRC_HARDWARETESTS_H_
#define SRC_HARDWARETESTS_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#ifdef _WIN32
	#include "AnaGateDllCan.h"
	#include "AnaGateDll.h"
	#include "tchar.h"
	#include "Winsock2.h"
	#include "windows.h"
#else
	#include <AnaGateDLL.h>
	#include <AnaGateDllCan.h>
	typedef unsigned long DWORD;
#endif


namespace HardwareTests_ns {

class HardwareTests {
public:
	HardwareTests();
	virtual ~HardwareTests();
	int test0();
	int test1();

};

} // namespace

#endif /* SRC_HARDWARETESTS_H_ */
