#!/bin/bash
# cleanup stale files after cmake
#make clean
find ./ -name "CMakeCache.txt" -exec rm -vf {} \;
find ./ -name "Makefile" -exec rm -vf {} \;
find ./ -name "cmake_install.cmake" -exec rm -vf {} \;
find ./ -type d -name "CMakeFiles" -exec rm -rvf {} \;
find ./ -type d -name "CanModule" -exec rm -rvf {} \;
find ./ -type d -name "CanInterfaceImplementations" -exec rm -rvf {} \;
# toplevel artifacts
find ./ -type d -name "bin" -exec rm -rvf {} \;
find ./ -type d -name "lib" -exec rm -rvf {} \;
