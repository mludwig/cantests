


#include <hardwareTests.h>
#include "../CanModule/CanInterface/include/VERSION.h"
#include "../../hardwareTester/CanModule/CanInterface/include/VERSION.h"
#include "../../hardwareTester/CanModule/CanLibLoader/include/CanLibLoader.h"
#include "../../hardwareTester/CanModule/LogIt/include/LogIt.h"

void usage( void ){
	cout << "hardwareTester: run specific tests with hardware" << endl;
	cout << "USAGE:" << endl;
	cout << " [ 0 ... N ] : run test 0 ... N with N=2" << endl;
	exit(0);
}


int main ( int argc, char **argv ){

	int runTest = -1;
	if ( argc < 2 ) usage();
	for ( int i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "0")) {
			runTest = 0;
		}
		if ( 0 == strcmp( argv[i], "1")) {
			runTest = 1;
		}
	}

	int ret = 0;
	try {
		switch ( runTest ){
		case 0: {
			HardwareTests_ns::HardwareTests cmd = HardwareTests_ns::HardwareTests();
			ret = cmd.test0();
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__;
			break;
		}
		default:{
			std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " test unknown";
			ret = -1;
			break;
		}
		}
	}
	catch  (const char* msg ){
		std::cout << __FUNCTION__ << " failed specific exception" << std::endl;
		cerr << msg << endl;
		return(-1);
	}
	catch (...){
		std::cout << __FUNCTION__ << " failed general exception" << std::endl;
		return(-2);
	}

	if ( ret == 0 ){
		std::cout << __FUNCTION__ << " success" << std::endl;
	} else {
		std::cout << __FUNCTION__ << " failed without exception" << std::endl;
	}
	return( ret );
}
