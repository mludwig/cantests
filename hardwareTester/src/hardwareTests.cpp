/*
 * hardware<tests.cpp
 *
 * run hardware specific tests using CanModule for QA
 *
 *  Created on: 27 July 2020
 *      Author: mludwig
 *
 */

#include "../include/hardwareTests.h"
#include "../../hardwareTester/CanModule/CanLibLoader/include/CanLibLoader.h"

#ifdef _WIN32
	#include "AnaGateDllCan.h"
	#include "AnaGateDll.h"
	#include "tchar.h"
	#include "Winsock2.h"
	#include "windows.h"
#else
	#include "AnaGateDLL.h"
	#include "AnaGateDllCan.h"
	typedef unsigned long DWORD;
#endif

namespace HardwareTests_ns {

HardwareTests::HardwareTests() {}

HardwareTests::~HardwareTests() {}



/**
 * - test port numbering for socketcan
 * - single thread/task plus some send/receive
 * - library and port instances
 * - defaults
 *
 * OS: cc7, linux with socketcan or windows
 * hardware:
 * a) one systec16
 * or
 * b) two peak duo
 * connections:
 * a) systec16 to USB0, can0 and can2 flatband cable
 * or
 * b) peak0 to USB0 and peak1 to USB1, global:can0 (=peak0::can0) and global:can2 (=peak1:can0) flatband cable
 *
 */
int HardwareTests::test0(){

	std::cout << __FUNCTION__;
	CanMessage msg = CanMessage();

	// connect two modules
	std::string vendor = "sock";
	std::string name0 = "sock:0"; // 1st port first systec16, or 1st port 1st peak
	std::string param = "Unspecified"; // defaults
	std::string name1 = "sock:2"; // 3rd port first systec16, or 1st port 2nd peak
	std::cout << __FUNCTION__ << " " << __FILE__ << " " << __LINE__ << std::cout;

	// create ONE lib access
	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( vendor );
	std::cout << __FUNCTION__ << " " << __FILE__ << " " << __LINE__ << std::cout;

	if ( lib0 == 0 ){
		throw "lib instance invalid";
	}

	// create 2 ports
	CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param );
	std::cout << __FUNCTION__ << " " << __FILE__ << " " << __LINE__ << std::cout;
	CanModule::CCanAccess* can1 = lib0->openCanBus( name1, param );
	std::cout << __FUNCTION__ << " " << __FILE__ << " " << __LINE__ << std::cout;

	if ( can0 == 0 or can1 == 0 ){
		throw "port instances are invalid";
	}


	// check nb instances
	std::cout << __FUNCTION__ << " have " << CanModule::CCanAccess::instanceCount() << " CCanAccess (can port) instances"<< std::endl;
	std::cout << __FUNCTION__ << " have " << CanModule::CanLibLoader::instanceCount() << " CanLibLoader (lib) instances"  << std::endl;
	if ( CanModule::CanLibLoader::instanceCount() != 1 ){
		throw "do not have the correct nb lib instances (need 1)";
	}
	if ( CanModule::CCanAccess::instanceCount() != 2 ){
		throw "do not have the correct nb access instances (need 2)";
	}

	can0->sendMessage( &msg );
	can1->sendMessage( &msg );

	std::cout << __FUNCTION__ << " success" << std::endl;
	return(0);
}

int HardwareTests::test1(){

	std::cout << __FUNCTION__ << " two bridges anagate, two libs, 1 task, 1 thread" << std::endl;
	CanMessage msg = CanMessage();

	// connect two modules
	std::string vendor0 = "an";
	std::string name0 = "an:1:137.138.12.99";  // anagate module at ip, can port 1
	std::string param0 = "Unspecified"; // defaults

	std::string vendor1 = "an";
	std::string name1 = "an:1:137.138.12.99";  // anagate module at ip, can port 1
	std::string param1 = "Unspecified"; // defaults

	CanModule::CanLibLoader* lib0 = CanModule::CanLibLoader::createInstance( vendor0 );
	CanModule::CCanAccess* can0 = lib0->openCanBus( name0, param0 );
	CanModule::CanLibLoader* lib1 = CanModule::CanLibLoader::createInstance( vendor1 );
	CanModule::CCanAccess* can1 = lib1->openCanBus( name1, param1 );

	// check
	std::cout << __FUNCTION__ << " have " << CanModule::CCanAccess::instanceCount() << " CCanAccess (can port) instances"<< std::endl;
	std::cout << __FUNCTION__ << " have " << CanModule::CanLibLoader::instanceCount() << " CanLibLoader (lib) instances"  << std::endl;

	can0->sendMessage( &msg );
	can1->sendMessage( &msg );

	std::cout << __FUNCTION__ << " success" << std::endl;
	return(0);
}



} /* namespace SpecialCommands_ns */
