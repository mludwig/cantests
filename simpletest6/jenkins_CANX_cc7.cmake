# toolchain for cc7 CANX
# mludwig at cern dot ch
# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CANX_cc7.cmake .
#
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for CANX-tester")
#
# boost: 1.69.0 as installed on the slave
#
SET ( BOOST_HOME /opt/3rdPartySoftware/boost_1_69_0d )
SET ( BOOST_PATH_LIBS ${BOOST_HOME}/stage/lib )
SET ( BOOST_HEADERS ${BOOST_HOME} )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_PATH_LIBS= ${BOOST_PATH_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_LIBS= ${BOOST_LIBS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: BOOST_HEADERS= ${BOOST_HEADERS} ")

# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "$ENV{JENKINSWS}/CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "$ENV{JENKINSWS}/CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_HEADERS= ${LOGIT_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LOGIT_PATH_LIBS= ${LOGIT_PATH_LIBS} ")

#
# xerces-c: must be installed on local machine
#
SET ( XERCES_PATH_LIBS "/usr/local/lib" )
SET ( XERCES_HEADERS "/usr/local/include" )
SET ( XERCES_LIBS "libxerces-c-3.1.so" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")
#
# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# systec: we use socketcan for linux
#
SET(SYSTEC_HEADERS "/usr/local/include")
SET(SYSTEC_LIB_PATH "/usr/local/lib")
SET(SYSTEC_LIB_FILE "-lsocketcan")
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_LIB_PATH  
	/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGate-1.0.9/linux64/gcc4_6 
)
SET ( ANAGATE_HEADERS "/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_LIB_PATH= ${ANAGATE_LIB_PATH} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_HEADERS= ${ANAGATE_HEADERS} " )
message( STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: ANAGATE_LIB_FILE= ${ANAGATE_LIB_FILE} " )
#
# peak: we use socketcan for linux
#
SET(PEAK_HEADERS "/usr/local/include")
SET(PEAK_LIB_PATH "/usr/local/lib")
SET(PEAK_LIB_FILE "-lsocketcan")
#
#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS 
	/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6
	/opt/3rdPartySoftware/Anagate/CAN/libAnaGate-1.0.9/linux64/gcc4_6 
)
SET ( SPECIAL_HEADERS ${ANAGATE_INC_DIR} 
	/opt/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include
)	
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease )

