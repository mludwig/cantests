/**
 *
 * Name        : simpletest0.cpp
 Author      :
 Version     :
 Copyright   : CERN
 Description : PEAK: OPCUA-1735 : deterministic socket can ports for programmed
device IDs, use system call to udev
============================================================================

 */

#include <iostream>

#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>
#include <LogIt.h>


using namespace std;

/**
 * testing CanModule for sw disconnect and reconnect, and for double connection/disconnection.
 * HW reconnect is NOT tested here.
 */
int main( int argc, char **argv ) {
	CanModule::CCanAccess *_cca0 = NULL;
	CanModule::CCanAccess *_cca1 = NULL;
	CanModule::CCanAccess *_cca2 = NULL;
	CanModule::CCanAccess *_cca3 = NULL;
	CanModule::CCanAccess *_cca4 = NULL;
	CanModule::CCanAccess *_cca5 = NULL;
	CanModule::CanLibLoader *_libloader = NULL;

	Log::LOG_LEVEL loglevel = Log::TRC;
	Log::initializeLogging( loglevel );
	LogItInstance *logIt = LogItInstance::getInstance();

	cout << __FILE__ << " " << __LINE__ << " register log it" << endl;
	logIt->registerLoggingComponent("CanModule", Log::TRC );

	cout << __FILE__ << " " << __LINE__ << " load lib" << endl;

	_libloader = CanModule::CanLibLoader::createInstance( "sock" );


	cout << endl << "===" << __FILE__ << " " << __LINE__ << " create bus" << endl;
	// linux  peak

	/**
	 * we need to distinguish peak from systec here. The ':deviceNNN' part of the port
	 * identifier is only present for PEAK.
	 */
	_cca0 = _libloader->openCanBus( "sock:can0:device8910", "125000" );
	_cca1 = _libloader->openCanBus( "sock:can1:device8910", "125000" );
	_cca2 = _libloader->openCanBus( "sock:can0", "125000" );
	_cca3 = _libloader->openCanBus( "sock:can1", "125000" );
	_cca4 = _libloader->openCanBus( "sock:can0:device9054", "125000" );
	_cca5 = _libloader->openCanBus( "sock:can1:device9054", "125000" );

	CanMessage cmsg;
	cmsg.c_id = 12;
	cout << __FILE__ << " " << __LINE__ << " send msg" << endl;
	for ( int k = 2; k > 0; k--){
		_cca0->sendMessage( &cmsg );
		_cca1->sendMessage( &cmsg );
		_cca2->sendMessage( &cmsg );
		_cca3->sendMessage( &cmsg );
		_cca4->sendMessage( &cmsg );
		_cca5->sendMessage( &cmsg );
	}

	// sleep( 2 );

	cout << __FILE__ << " " << __LINE__ << " explicitly close buses" << endl;
	_libloader->closeCanBus( _cca0);
	_libloader->closeCanBus( _cca1);
	_libloader->closeCanBus( _cca2);
	_libloader->closeCanBus( _cca3);
	_libloader->closeCanBus( _cca4);
	_libloader->closeCanBus( _cca5);

	return 0;
}
