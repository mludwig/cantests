# © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
#
#   Created on: Wed Aug 30 10:15:57 CEST 2017
# 
#  		Author: Michael Ludwig <michael.ludwig@cern.ch>
#       Contributors and prior art: Benjamin Farnham, Piotr Nikiel, Viacheslav Filimonov
# 
#  This file is part of the CAN Common Tools project and is the property of CERN, Geneva, Switzerland,
#  and is not free software, since it builds on top of vendor
#  specific communication interfaces and architectures, which are generally non-free and
#  are subject to licensing and/or registration. Please refer to the relevant
#  collaboration agreements between CERN ICS and the vendors for further details.
# 
#  The non-vendor specific parts of the software can be made available on request
#  under the GNU Lesser General Public Licence,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
# 
# CMakeLists.txt for CANX, use with a build toolchain
cmake_minimum_required(VERSION 3.0)
project( simpletest0 LANGUAGES C CXX  VERSION 1.1.9.10  ) # sets PROJECT_VERSION etc etc
message(STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CANX version= ${PROJECT_VERSION}" )
file(WRITE include/VERSION.h "// VERSION.h - do not edit\n#define CANX_VERSION \"${PROJECT_VERSION}\"" )

SET( CMAKE_BUILD_TYPE "Debug" )
SET( CMAKE_VERBOSE_MAKEFILE on )
SET( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
SET( CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
SET( CMAKE_COLOR_MAKEFILE "ON" )
SET( CMAKE_CXX_STANDARD 11 ) 
SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
# SET( CMAKE_EXPORT_COMPILE_COMMANDS ON ) # produce a compile_commands.json file
  
macro(print_all_variables)
    message(STATUS "print_all_variables------------------------------------------{")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        message(STATUS "${_variableName}=${${_variableName}}")
    endforeach()
    message(STATUS "print_all_variables------------------------------------------}")
endmacro()

# pull in CanModule sources, and LogIt from inside
function ( clone_CanModule )  
 SET (CANMODULE_VERSION "master" )  
 # SET (CANMODULE_VERSION "master" )  
 SET (CANMODULE_REPO "https://github.com/quasar-team/CanModule.git")
 
  message(STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: cloning CanModule from gitlab ${CANMODULE_REPO}:  *NOTE* cloning version [${CANMODULE_VERSION}]")
  execute_process(COMMAND git clone -b ${CANMODULE_VERSION} ${CANMODULE_REPO} WORKING_DIRECTORY ${PROJECT_BINARY_DIR}  )
  include_directories( ${CMAKE_SOURCE_DIR}/CanModule/include )
  message(STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CanModule cloned from repo ${CANMODULE_REPO} ")
endFunction()

function ( CANX_makeVersion )
  message(STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CANX_makeVersion")
  execute_process(COMMAND ./make_version.sh ) 
endFunction()

# compiler/build flags
IF (WIN32)
  	message( STATUS " ${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Windows build" )
	add_definitions(-DSUPPORT_XML_CONFIG -W0 -DWIN32_LEAN_AND_MEAN )
	add_definitions(-D_WIN32_WINDOWS)
	add_definitions("/EHsc")
	
	# dynamic linking
	SET(CMAKE_CXX_FLAGS_RELEASE "/MD")
	SET(CMAKE_CXX_FLAGS_DEBUG "/MDd /Zi")
	
	SET( CMAKE_EXE_LINKER_FLAGS     "${CMAKE_EXE_LINKER_FLAGS}    /MACHINE:X64" )
	SET( CMAKE_SHARED_LINKER_FLAGS  "${CMAKE_SHARED_LINKER_FLAGS} /MACHINE:X64" )
	SET( CMAKE_STATIC_LINKER_FLAGS  "${CMAKE_STATIC_LINKER_FLAGS} /MACHINE:X64" )
	SET( CMAKE_MODULE_LINKER_FLAGS  "${CMAKE_MODULE_LINKER_FLAGS} /MACHINE:X64" )
	
	find_package(Threads REQUIRED)
ELSE()
  	message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Linux build" )
	SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC" )
	add_definitions( "-Wall -Wno-deprecated -std=gnu++0x -Wno-literal-suffix -D_CERN_CC7 -O2 -DBOOST_LOG_DYN_LINK" )
ENDIF()

#
# Load build toolchain file and internal checks before we start building
#
if( DEFINED CMAKE_TOOLCHAIN_FILE )
  message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CMAKE_TOOLCHAIN_FILE is defined -- including [${CMAKE_TOOLCHAIN_FILE}]")
  include( ${CMAKE_TOOLCHAIN_FILE} )    
endif()
if( NOT DEFINED BOOST_PATH_LIBS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable BOOST_PATH_LIBS has not been defined.")
endif() 
if( NOT DEFINED BOOST_LIBS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable BOOST_LIBS has not been defined.")
endif() 
if( NOT DEFINED BOOST_HEADERS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable BOOST_HEADERS has not been defined." )
endif() 
if( NOT DEFINED XERCES_HEADERS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable XERCES_HEADERS has not been defined." )
endif() 
if( NOT DEFINED XERCES_PATH_LIBS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable XERCES_PATH_LIBS has not been defined." )
endif() 
if( NOT DEFINED XERCES_LIBS )
  message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable XERCES_LIBS has not been defined." )
endif() 
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain injected [BOOST_PATH_LIBS:${BOOST_PATH_LIBS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain injected [BOOST_HEADERS:${BOOST_HEADERS}]" )
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] toolchain injected [BOOST_LIBS:${BOOST_LIBS}]" )

include_directories ( ${BOOST_HEADERS} )
link_directories( ${BOOST_PATH_LIBS} )

#
# since we build CanModule from sources, we need to specify the according vendor libs in the toolchain.
# we check if the toolchain is correctly doing that
#
if( (NOT DEFINED SYSTEC_LIB_PATH) OR (NOT DEFINED SYSTEC_HEADERS) OR (NOT DEFINED SYSTEC_LIB_FILE)  )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable SYSTEC_HEADERS has not been defined." )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable SYSTEC_LIB_PATH has not been defined." )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable SYSTEC_LIB_FILE has not been defined." )
else()
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain SYSTEC_HEADERS= ${SYSTEC_HEADERS}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain SYSTEC_LIB_PATH= ${SYSTEC_LIB_PATH}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain SYSTEC_LIB_FILE = ${SYSTEC_LIB_FILE}" )
endif() 

if( (NOT DEFINED PEAK_LIB_PATH) OR (NOT DEFINED PEAK_HEADERS) OR (NOT DEFINED PEAK_LIB_FILE)  )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable PEAK_LIB_PATH has not been defined." )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable PEAK_HEADERS has not been defined." )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Required variable PEAK_LIB_FILE has not been defined." )
else()
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain PEAK_LIB_PATH= ${PEAK_LIB_PATH}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain PEAK_HEADERS= ${PEAK_HEADERS}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain PEAK_LIB_FILE = ${PEAK_LIB_FILE}" )
endif() 

if( (NOT DEFINED ANAGATE_LIB_PATH) OR (NOT DEFINED ANAGATE_HEADERS) OR (NOT DEFINED ANAGATE_LIB_FILE)  )
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: required variable ANAGATE_LIB_PATH has not been defined." ) 
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: required variable ANAGATE_HEADERS has not been defined." ) 
	message(FATAL_ERROR "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: required variable ANAGATE_LIB_FILE has not been defined." ) 
else()
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain ANAGATE_LIB_PATH= ${ANAGATE_LIB_PATH}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain ANAGATE_HEADERS= ${ANAGATE_HEADERS}" )
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: toolchain ANAGATE_LIB_FILE = ${ANAGATE_LIB_FILE}" )
endif() 


#
# before we actually clone CanModule, we set all options right
#
option(STANDALONE_BUILD "Build it as a stand-alone library instead of for Quasar" ON )
option(STANDALONE_BUILD_SHARED "Build it as a shared library instead of static" ON )
SET( LOGIT_BUILD_OPTION "LOGIT_AS_INT_SRC" CACHE STRING "LogIt is a mandatory dependency of CanModule. Inclusion options LOGIT_AS_INT_SRC, LOGIT_AS_EXT_SHARED, LOGIT_AS_EXT_STATIC")
set_property( CACHE LOGIT_BUILD_OPTION PROPERTY STRINGS LOGIT_AS_INT_SRC LOGIT_AS_EXT_SHARED LOGIT_AS_EXT_STATIC)
option(LOGIT_BACKEND_STDOUTLOG "The basic back-end: logs to stdout" ON )
option(LOGIT_BACKEND_BOOSTLOG "Rotating file logger back-end: fixed size on disk based on boost logging library" ON )
option(LOGIT_BACKEND_UATRACE "UnifiedAutomation toolkit logger" OFF )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: LogIt build option LOGIT_BUILD_OPTION [${LOGIT_BUILD_OPTION}]")

#
# clone CanModule, and LogIt from inside, and add them to the sources in the build. 
#
clone_CanModule()
add_subdirectory( ${PROJECT_SOURCE_DIR}/CanModule )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CanModule added as source code from sub-directory CanModule")
#
# LogIt is used by CANX directly as well
#
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: include_directories LOGIT_HEADERS [${LOGIT_HEADERS}]")
include_directories ( ${LOGIT_HEADERS} )

#
# CanModule headers
#
SET ( CANMODULE_SOURCE_DIR ${PROJECT_SOURCE_DIR}/CanModule )	
include_directories ( 
		${CANMODULE_SOURCE_DIR}/CanInterface/include
	 	${CANMODULE_SOURCE_DIR}/CanInterfaceImplementations/include
	 	${CANMODULE_SOURCE_DIR}/CanInterfaceImplementations/unitTestMockUpImplementation
	 	${CANMODULE_SOURCE_DIR}/CanLibLoader/include
	 	${CANMODULE_SOURCE_DIR}/LogIt/include 
	 )

#
# CANX specific sources
#
SET ( CANX_SOURCES ${PROJECT_SOURCE_DIR}/src/simpletest4.cpp ) 
include_directories ( ${PROJECT_SOURCE_DIR}/include ) 

#
IF (WIN32)
	set ( SYSLIBS ${CMAKE_THREAD_LIBS_INIT} )
	include_directories ( ${XERCES_HEADERS} )
	link_directories( ${XERCES_PATH_LIBS} )
ELSE()
	set ( SYSLIBS "-ldl -lc -lm -lz -lpthread")		
ENDIF()

	
# libraries needed for special and direct functionality where we bypass CanModule and do some
# one-shot-testing or special sequences: i.e to investigate external reset behavior.
# set by toolchains
IF (WIN32)
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Windows build")
	SET ( SPECIAL_PATH_LIBS ${ANAGATE_LIB_PATH} )
	SET ( SPECIAL_HEADERS ${ANAGATE_HEADERS} )
	SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} )
ELSE()
	message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: Linux build")
	SET ( SPECIAL_PATH_LIBS ${ANAGATE_LIB_PATH} )
	SET ( SPECIAL_HEADERS ${ANAGATE_HEADERS} )
	SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} )
ENDIF()
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] SPECIAL_HEADERS= ${SPECIAL_HEADERS}")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] ANAGATE_HEADERS= ${ANAGATE_HEADERS}")

include_directories ( ${SPECIAL_HEADERS} )
link_directories( ${SPECIAL_PATH_LIBS} )

SET ( TARGET_NAME simpletest4 )
add_executable ( ${TARGET_NAME} ${CANX_SOURCES} )
target_include_directories( ${TARGET_NAME} PUBLIC 
		${ANAGATE_HEADERS}
	 	${SOCKETCAN_HEADERS} 
		${SYSTEC_HEADERS}
		${PEAKCAN_HEADERS}
		${SPECIAL_HEADERS} 
		${XERCES_HEADERS}
		${BOOST_HEADERS}
	)

# we load the vendor specific libs at CANX runtime, which are linked from CanModule
target_link_libraries ( ${TARGET_NAME} CanModule ${SYSLIBS} ${BOOST_LIBS} ${XERCES_LIBS} ${SPECIAL_LIB_FILES} )
