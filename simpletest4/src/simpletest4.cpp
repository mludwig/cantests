//============================================================================
// Name        : simpletest4.cpp
// Author      : mludwig
// Version     :
// Copyright   : CERN 2020
// Description : test for anagate shut down segfault OPCUA-1696
//============================================================================

#include <iostream>

#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>
#include <LogIt.h>


using namespace std;
/**
 * string hex representation to uchar conversion
 */
uint8_t _stringToUChar( string s ){
	int ii = 0;
	sscanf( s.c_str(), "%x", &ii );
	return( (uint8_t) ii );
}

/**
 * string hex representation to long int conversion
 */
int64_t _stringToLongInt( string s ){
	int64_t ii = 0;
	sscanf( s.c_str(), "%lx", &ii );
	return( ii );
}

#if 0
typedef struct {
	short cobID;
	unsigned char len;
	unsigned char message[ 16 ];
	bool rtr;
} msg_t;
#endif
/**
 * the frame.data specified is actually the whole can message, including ID, flags etc etc.
 * The frame is a string, with dots as delimiters:
 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
 * RTR = 1 char representing a boolean RTR, 1 bit
 * IDE = 1 char representing a boolean IDE, 1 bit
 * R0  = 1 char representing a boolean R0, 1 bit
 * DLC = 1 char representing hex range 0...f, 4 bit
 * DATA = 16 chars representing in hex 8 byte, 64bit
 *
 * and we do not send those:
 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
 * ACK = 1 char for 1 bit
 * =====
 * makes 28 chars in the string, plus the 7 dots = 35
 *
 * SOF, EOF and IFS not specified
 *
 * i.e.: "004.0.0.0.3.F80001"
 */
CanMessage _standardCanMsgFromFrame( std::string frame ){
	LOG( Log::INF ) << " _standardCanMsgFromFrame: " << frame;
	CanMessage cm;
	string ID  = frame.substr(0,3);
	string RTR = frame.substr(4,1);
	string IDE = frame.substr(6,1);
	string R0  = frame.substr(8,1);
	string DLC = frame.substr(10,1);
	string DATA = frame.substr(12,16);

	//string CRC = frame.substr(30,4);
	//string ACK = frame.substr(35,1);

	//#if 0
	LOG( Log::INF ) << "ID=" << ID
			<< " RTR="<< RTR
			<< " IDE="<<IDE
			<< " R0="<<R0
			<< " DLC="<<DLC
			<< " DATA="<<DATA;
	//	<< " CRC="<<CRC
	//	<< " ACK="<<ACK;
	//#endif
	cm.c_id   = _stringToLongInt( ID );
	cm.c_dlc  = _stringToUChar( DLC );
	cm.c_rtr  = _stringToUChar( RTR );
	cm.c_ff   = _stringToUChar( IDE );
	if ( cm.c_dlc > 8 ) {
		LOG( Log::INF ) << " detected a specified CAN frame with more than 8 byte length ["
				<< cm.c_dlc << "] , force it to 8 for now. Check your config file please.";
		cm.c_dlc = 8;
	}
	for ( uint8_t i = 0; i < cm.c_dlc; i++ ){
		cm.c_data[ i ] = _stringToUChar( DATA.substr(2*i,2) ); // per byte = 2chars in hex
	}
	return( cm );
}



void usage( string ar ){
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << ar << " [systec || anagate || peak]" << endl;
	exit(0);
}
#if 0
msg_t createMessage( short cobID, unsigned char len, unsigned char *message, bool rtr ){
	msg_t m;
	m.cobID = cobID;
	m.len = len;
	m.rtr = rtr;

	for ( int i = 0; i < 16; i++ ){
		m.message[ i ] = '\0';
	}

	// len=nb bytes = 2* nb nibbles = 2 * nb of characters in string
	for ( int i = 0; i < (int) len * 2; i++ ){
		m.message[ i ] = message[ i ];
	}
	return m;
};
#endif

/**
 * testing CanModule for clean exiting
 */
int main( int argc, char **argv ) {

	vector<CanMessage> messages;

	/** ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
	 * ID.RTR.IDE.R0.DLC.DATA.CRC.ACK
	 * ID  = 3 chars representing hex range 000...7ff for ID, 11 bit
	 * RTR = 1 char representing a boolean RTR, 1 bit
	 * IDE = 1 char representing a boolean IDE, 1 bit
	 * R0  = 1 char representing a boolean R0, 1 bit
	 * DLC = 1 char representing hex range 0...f, 4 bit
	 * DATA = 16 chars representing in hex 8 byte, 64bit
	 * CRC = 4 chars representing hex range 0000...ffff, 16 bit
	 * ACK = 1 char for 1 bit
	 * =====
	 * makes 28 chars in the string, plus the 7 dots = 35
	 */
	messages.push_back( _standardCanMsgFromFrame( "004.0.0.0.1.C8" ));
	messages.push_back( _standardCanMsgFromFrame( "004.0.0.0.1.CC" ));
	messages.push_back( _standardCanMsgFromFrame( "004.0.0.0.1.C4" ));
	messages.push_back( _standardCanMsgFromFrame( "004.0.0.0.3.F80001" ));

	try {
		CanModule::CCanAccess *_cca0 = NULL;
		CanModule::CanLibLoader *_libloader = NULL;

		Log::LOG_LEVEL loglevel = Log::TRC;
		Log::initializeLogging( loglevel );
		LogItInstance *logIt = LogItInstance::getInstance();

		cout << __FILE__ << " " << __LINE__ << " register log it" << endl;
		logIt->registerLoggingComponent("CanModule", Log::TRC );

		cout << __FILE__ << " " << __LINE__ << " load lib" << endl;

		// linux & windows anagate
		/**
		 * MLOGANA(ERR, this) << "  p0 = baud rate, 125000 or whatever the module supports";
		 * MLOGANA(ERR, this) << "  p1 = operation mode";
		 * MLOGANA(ERR, this) << "  p2 = termination";
		 * MLOGANA(ERR, this) << "  p3 = high speed";
		 * MLOGANA(ERR, this) << "  p4 = time stamp";
		 */
		_libloader = CanModule::CanLibLoader::createInstance( "an" );
		_cca0 = _libloader->openCanBus( "an:can0:128.141.159.194", "250000 0 1 1 0" );

		// socket
		//_libloader = CanModule::CanLibLoader::createInstance( "sock" );
		//_cca0 = _libloader->openCanBus( "sock:can0", "250000" );



#ifdef _WIN32
//		Sleep( 2000 );
#else
//		sleep( 2 );
#endif


		/**
		 * send some messages
		 */
		int repeat = 100;
		for ( int i = repeat; i > 0; i-- ){
			cout << "countdown= " << i << " " << endl;
			for ( unsigned int k = 0; k < messages.size(); k++ ){
				_cca0->sendMessage( &(messages[k]) );
			}
		}
		// cout << __FILE__ << " " << __LINE__ << " do not close  bus" << endl;
		cout << __FILE__ << " " << __LINE__ << " explicitly close bus" << endl;
		_libloader->closeCanBus( _cca0);
	}
	catch (...){
		cout << __FILE__ << " " << __LINE__ << " caught exception, trying next" << endl;
	}
	return 0;
}
