//============================================================================
// Name        : simpletest0.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>
#include <LogIt.h>

#include <boost/thread/thread.hpp>
#include <boost/chrono.hpp>

using namespace boost::chrono;
using namespace std;

#include <CpuDelay.h>


/**
 * demo a calibrated cpu delay loop
 */
int main() {
	Log::LOG_LEVEL loglevel = Log::TRC;
	Log::initializeLogging( loglevel );
	LogItInstance *logIt = LogItInstance::getInstance();

	cout << __FILE__ << " " << __LINE__ << " register log it" << endl;
	logIt->registerLoggingComponent("CanModule", Log::TRC );

	// create one delay loop which calibrates itself at creation
	CpuDelay *dd = new CpuDelay();

	boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
	dd->delay_ms( 1 );
	boost::chrono::system_clock::time_point stop = boost::chrono::system_clock::now();
	boost::chrono::duration<double> sec = stop - start;
	double msec = sec.count() * 1000;
	std::cout << "calibrated delay 1ms took " << msec << " ms\n";

	start = boost::chrono::system_clock::now();
	dd->delay_us( 10 );
	stop = boost::chrono::system_clock::now();
	sec = stop - start;
	double usec = sec.count() * 1000000;
	std::cout << "calibrated delay 10us took " << usec << " us\n";


	return 0;
}
