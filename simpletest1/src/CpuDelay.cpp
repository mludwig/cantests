/*
 * cpuDelay.cpp
 *
 *  Created on: Oct 21, 2019
 *      Author: mludwig
 */

#include <boost/chrono.hpp>
#include <CpuDelay.h>
#include <iostream>
#include <cmath>

using namespace boost::chrono;

CpuDelay::CpuDelay():
_calibrateLoop_ms( 50000 ),
_calibrateLoop_us( 100 ),
_dummy(0)
{
	std::cout << "present internal delay ms= " << _calibrateLoop_ms << std::endl;
	std::cout << "present internal delay us= " << _calibrateLoop_us << std::endl;

	// we create a calibration straight away, and we do it 10 times to get a good value
	std::cout << "calibrating ms delay ... " << std::endl;
	int sumLoop = 0;
	for ( int n = 0; n < 10; n++ ){
		boost::chrono::system_clock::time_point start0 = boost::chrono::system_clock::now();
		for ( int k = 0; k < 1000; k++ ) _eatCpu();
		boost::chrono::system_clock::time_point stop0 = boost::chrono::system_clock::now();
		boost::chrono::duration<double> sec0 = stop0  - start0;
		double msec = sec0.count() * 1000;
		// std::cout << "calibrating delay ... " << msec << " ms\n";
		sumLoop += (int) ((double) _calibrateLoop_ms * 1000.0/msec);
	}
	_calibrateLoop_ms = (int) ((double ) sumLoop / 10.0);

	std::cout << "calibrating us delay ... " << std::endl;
	sumLoop = 0;
	int count = 0;
	for ( int n = 0; n < 50; n++ ){
		boost::chrono::system_clock::time_point start0 = boost::chrono::system_clock::now();
		for ( int k = 0; k < 1000; k++ ) _eatCpu_us();
		boost::chrono::system_clock::time_point stop0 = boost::chrono::system_clock::now();
		boost::chrono::duration<double> sec0 = stop0 - start0;
		double usec = sec0.count() * 1000000;
		// windows has "ratees"
		if ( usec > 1 ){
			// std::cout << "calibrating delay ... " << usec << " us\n";
			count++;
			sumLoop += (int) ((double) _calibrateLoop_us * 1000.0/usec);
		}
		//else {
		//	std::cout << "ratee detected .. skip " << usec << " us\n";
		//}
	}
	_calibrateLoop_us = (int) ((double ) sumLoop / count );

	std::cout << "new internal delay ms= " << _calibrateLoop_ms << std::endl;
	std::cout << "new internal delay us= " << _calibrateLoop_us << " from " << count << " samples)" << std::endl;
}
/**
 * eats CPU for delay_ms
 */
void CpuDelay::delay_ms( int delay_ms ){
	for ( int k = 0; k < delay_ms; k++ ) _eatCpu();
}
void CpuDelay::delay_us( int delay_us ){
	for ( int k = 0; k < delay_us; k++ ) _eatCpu_us();
}

void CpuDelay::_eatCpu( void ){
	_dummy = 0;
	for ( int i = 0; i < _calibrateLoop_ms; i++ ){
		double d0 = sqrt( sqrt( i * 7 + i) + sqrt( i * 7 + i + 1));
		_dummy += d0 + 1.99;
	}
}
void CpuDelay::_eatCpu_us( void ){
	_dummy = 0;
	for ( int i = 0; i < _calibrateLoop_us; i++ ){
		double d0 = sqrt( i );
		_dummy += d0 + 1.99;
	}
}


CpuDelay::~CpuDelay() {}

