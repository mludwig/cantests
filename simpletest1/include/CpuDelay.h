/*
 * cpuDelay.h
 *
 *  Created on: Oct 21, 2019
 *      Author: mludwig
 */

#ifndef SIMPLETEST1_CPUDELAY_H_
#define SIMPLETEST1_CPUDELAY_H_

class CpuDelay {
public:
	CpuDelay();
	virtual ~CpuDelay();
	void delay_ms( int delay_ms );
	void delay_us( int delay_us );

private:
	int _calibrateLoop_ms;
	int _calibrateLoop_us;

	double _dummy;
	void _eatCpu( void );
	void _eatCpu_us( void );
};

#endif /* SIMPLETEST1_CPUDELAY_H_ */
