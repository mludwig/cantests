# toolchain for cc7 CANX
# cmake -DCMAKE_TOOLCHAIN_FILE= <toolchainname>.cmake .
#
# the toolchain just sets variables, but does not actually DO anything
message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CC7 build for CANX" )
#
# boost
#
SET ( BOOST_PATH_LIBS "/home/mludwig/3rdPartySoftware/boost/1.64.0/stage/lib" )
SET ( BOOST_HEADERS   "/home/mludwig/3rdPartySoftware/boost/1.64.0" )
SET ( BOOST_LIBS 
	-lboost_log 
	-lboost_log_setup 
	-lboost_filesystem 
	-lboost_program_options 
	-lboost_system
	-lboost_chrono 
	-lboost_date_time 
	-lboost_thread  )
# 
# LogIt, used by CANX directly as well
#
SET ( LOGIT_HEADERS   "CanModule/LogIt/include" )
SET ( LOGIT_PATH_LIBS "CanModule/LogIt/lib" )
SET ( LOGIT_LIBS "-lLogIt" )
#
# xerces-c
#
SET ( XERCES_PATH_LIBS "../xerces-c/src/Debug" )
SET ( XERCES_HEADERS "../xerces-c/src" )
SET ( XERCES_LIBS "-lxerces-c" )
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_HEADERS= ${XERCES_HEADERS} ")
message(STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: XERCES_PATH_LIBS= ${XERCES_PATH_LIBS} ")

# CanModule build behaviour:
# CANMODULE_BUILD_VENDORS OFF or not specified: only build mockup, do not build any vendor libs (default phony)
# CANMODULE_BUILD_VENDORS ON, nothing else: build mockup and all vendor libs (default all on)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_SYSTEC OFF: build mockup and all vendor libs except systec (drop systec)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_ANAGATE OFF: build mockup and all vendor libs except anagate (drop anagate)
# CANMODULE_BUILD_VENDORS ON, CANMODULE_BUILD_PEAK OFF: build mockup and all vendor libs except peak (drop peak)
SET(CANMODULE_BUILD_VENDORS "ON" )
# disable a vendor
#SET(CANMODULE_BUILD_SYSTEC "OFF")
#SET(CANMODULE_BUILD_ANAGATE "OFF")
#SET(CANMODULE_BUILD_PEAK "OFF")
#
# we build CanModule from the sources, and we need headers and libs from the vendors
#
# linux: we use socketcan for systec and peak
#
SET( SOCKETCAN_HEADERS "/home/mludwig/CAN/CAN_libsocketcan/include" )
SET( SOCKETCAN_LIB_PATH "/home/mludwig/CAN/CAN_libsocketcan/src/.libs" )
SET( SOCKETCAN_LIB_FILE "-lsocketcan" )
#
SET( SYSTEC_HEADERS ${SOCKETCAN_HEADERS} )
SET( SYSTEC_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( SYSTEC_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
SET( PEAK_HEADERS ${SOCKETCAN_HEADERS} )
SET( PEAK_LIB_PATH ${SOCKETCAN_LIB_PATH} )
SET( PEAK_LIB_FILE ${SOCKETCAN_LIB_FILE} )
#
# anagate: we use a TCP convenience library from anagate
#
SET ( ANAGATE_LIB_PATH 
	"/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6" 
	"/home/mludwig/3rdPartySoftware/Anagate/CAN/libAnaGateExt-1.0.3/linux64/gcc4_6"
	"/home/mludwig/3rdPartySoftware/Anagate//CAN/libAnaGate-1.0.9/linux64/gcc4_6"
)
SET ( ANAGATE_HEADERS "/home/mludwig/3rdPartySoftware/Anagate/CAN/anagate-api-2.13/linux64/gcc4_6/include" )
SET ( ANAGATE_LIB_FILE "-lAPIRelease64 -lCANDLLRelease64" )

#
# special functions not using CanModule
#
SET ( SPECIAL_PATH_LIBS ${ANAGATE_PATH_LIBS} ) 
SET ( SPECIAL_HEADERS  ${ANAGATE_HEADERS} ) 
SET ( SPECIAL_LIB_FILES ${ANAGATE_LIB_FILE} -lAnaGateExtRelease -lAnaGateRelease )
