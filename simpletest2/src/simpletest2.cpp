//============================================================================
// Name        : simpletest2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include <CanLibLoader.h>
#include <CanBusAccess.h>
#include <CCanAccess.h>
#include <LogIt.h>


using namespace std;

void usage( string ar ){
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << ar << " [systec || anagate || peak]" << endl;
	exit(0);
}

/**
 * testing CanModule for port naming syntax rules as documented. No need for HW.
 */
int main( int argc, char **argv ) {

	bool systec_flag = false;
	bool anagate_flag = false;
	bool peak_flag = false;

	if ( argc < 2 ) usage( argv[0] );
	for ( int i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "systec")) systec_flag = true;
		if ( 0 == strcmp( argv[i], "anagate")) anagate_flag = true;
		if ( 0 == strcmp( argv[i], "peak")) peak_flag = true;
	}


	vector<string> connection = {
			{"sock:can0"},
			{"sock:0"},
			{"sock:vcan0"},
			{"sock:whatsovcanever0"},
			{"sock:whatsocanever0"},
			{"sock:vcanwhatsoever0"}
	};


	for ( uint32_t i = 0; i < connection.size(); i++ ){
		cout << endl << "==try out " << connection[i] << " ===" << endl;
		try{

			CanModule::CCanAccess *_cca0 = NULL;
			CanModule::CanLibLoader *_libloader = NULL;

			Log::LOG_LEVEL loglevel = Log::TRC;
			Log::initializeLogging( loglevel );
			LogItInstance *logIt = LogItInstance::getInstance();

			cout << __FILE__ << " " << __LINE__ << " register log it" << endl;
			logIt->registerLoggingComponent("CanModule", Log::TRC );

			cout << __FILE__ << " " << __LINE__ << " load lib" << endl;

#ifdef _WIN32
			if ( systec_flag && !anagate_flag){
				// windows systec
				_libloader = CanModule::CanLibLoader::createInstance( "st" );
			} else if ( peak_flag && !anagate_flag){
				// windows peak
				_libloader = CanModule::CanLibLoader::createInstance( "pk" );

			}
#else
			if ( (systec_flag || peak_flag) && !anagate_flag ){
				// linux systec & peak
				_libloader = CanModule::CanLibLoader::createInstance( "sock" );
			}
#endif

			if ( anagate_flag && (!systec_flag && !peak_flag) ){
				// linux & windows anagate
				_libloader = CanModule::CanLibLoader::createInstance( "an" );
			}

#ifdef _WIN32
			if ( systec_flag && !anagate_flag ){
				// windows systec
				_cca0 = _libloader->openCanBus( "st:can0", "125000" ); //sender
			} else if ( peak_flag && !anagate_flag ){
				// windows peak
				_cca0 = _libloader->openCanBus( "pk:can0", "Unspecified" );
			}
#else
			if ( systec_flag || peak_flag ){
				// linux systec & peak
				_cca0 = _libloader->openCanBus( connection[ i ], "125000" ); // sender
			}
#endif
			if ( anagate_flag && (!systec_flag && !peak_flag) ){
				// linux & windows anagate
				_cca0 = _libloader->openCanBus( "an:can0:128.141.159.194", "125000 0 0 0 0" ); //sender
			}
#ifdef _WIN32
			Sleep( 2000 );
#else
			sleep( 2 );
#endif

			cout << __FILE__ << " " << __LINE__ << " explicitly close bus" << endl;
			_libloader->closeCanBus( _cca0);
		}
		catch (...){
			cout << __FILE__ << " " << __LINE__ << " caught exception, trying next" << endl;
		}
	}
#ifdef _WIN32
	Sleep( 2000 );
#else
	sleep( 2 );
#endif
	return 0;
}
